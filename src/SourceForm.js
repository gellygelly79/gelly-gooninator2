import React, { Component, PropTypes } from 'react';
import store from "./Store";


const timingFunctionLabels = {
  assault: "60fps",
  variableMedium: "Fast (0.2s-1.2s, varying)",
  variableFast: "Really fast (0.0s-0.6s, varying)",
  variableSlow: "Slow - Recommended for captions! - (3.4-5.4s, varying)",
  variableSlowest: "Slowest - Recommended for captions! - (varying)",
  fixed05: "0.5s",
  fixed1: "1.0s",
  fixed2: "2.0s",
  cockzoom: "5.0s with image zooming",
  cockzoomv2: "2.0s with image zooming",
  cockzoomv3: "1.0s with image zooming",
}


class Source extends Component {
  static propTypes = {
    source: PropTypes.string.isRequired,
    isFavorite: PropTypes.bool,
  }

  render() {
    const remove = store.mutator((oldState) => {
      const sources = oldState.sources.filter((oldSource) => oldSource !== this.props.source);
      return {
        ...oldState,
        sources,
        isRunning: oldState.isRunning && sources.length > 0,
      };
    });
    const toggleFavorite = store.mutator((oldState) => {
      return {
        ...oldState,
        favorites: {
          ...oldState.favorites,
          [this.props.source]: !this.props.isFavorite,
        },
      };
    }, (newState) => {
      const ga = window.ga; if (!ga) return;
      ga('send', {
        hitType: 'event',
        eventCategory: 'Favoriting',
        eventAction: newState.favorites[this.props.source] ? 'favorite' : 'unfavorite',
        eventLabel: this.props.source,
      });
    });
    return (
      <div className="gn-source gn-form-entry">
        <span className="gn-source-name" onClick={toggleFavorite} style={{cursor: 'pointer'}}>
          {this.props.source} {this.props.isFavorite ? "★": "☆"}
        </span>
        <span className="gn-source-remove-button" onClick={remove}>-</span>
      </div>
    );
  }
}


class SourceTextInput extends Component {
  constructor() {
    super();
    this.state = {value: ""};

  }

  onChange = (e) => {
    this.setState({value: e.target.value});
  }

  componentDidMount() {
    if (this.props.sources.length < 1) {
      this.inputEl.focus();
    }
  }

  render() {
    const submit = store.mutator((oldState) => {
      const newSource = this.state.value.trim();
      if (newSource.length < 1) return oldState;
      const sources = newSource
        .split(" ")
        .map((s) => s.trim())
        .filter((s) => s.length);

      this.setState({value: ""});
      return {
        ...oldState,
        sources: oldState.sources.concat(sources),
      }
    });
    return (
      <form className="gn-source-text-input gn-form-entry" onSubmit={submit}>
        <input type="text"
               ref={(el) => this.inputEl = el}
               placeholder="enter a tumblr"
               value={this.state.value}
               onChange={this.onChange} />
        <span className="gn-source-add-button" onClick={submit}>+</span>
      </form>
    );
  }
}


class TimingFunctionPicker extends Component {
  onChange = (e) => {
    store.setter('timingFunction', e.target.value)();
  };

  render() {
    const options = [
      'variableMedium',
      'variableFast',
      'variableSlow',
      'variableSlowest',
      'cockzoom',
      'cockzoomv2',
      'cockzoomv3',
      'assault',
      'fixed05',
      'fixed1',
      'fixed2',
    ];

    return (
      <select value={this.props.timingFunction} onChange={this.onChange}>
        {options.map((option) => {
          return <option key={option} value={option}>{timingFunctionLabels[option]}</option>;
        })}
      </select>
    );
  }
}


class GifsOrStillsPicker extends Component {
  static propTypes = {
    includeGifs: PropTypes.bool.isRequired,
    includeStills: PropTypes.bool.isRequired,
  };

  onChange = (e) => {
    store.setState({
        ...store.state,
        includeGifs: e.target.value === "all" || e.target.value === "gifs",
        includeStills: e.target.value === "all" || e.target.value === "stills",
    })
  };

  render() {
    const value = this.props.includeGifs
    ? this.props.includeStills
      ? "all"
      : "gifs"
    : "stills";
    return (
      <select value={value} onChange={this.onChange}>
        <option key="all" value="all">All images</option>
        <option key="gifs" value="gifs">Only gifs</option>
        <option key="stills" value="stills">Only stills</option>
      </select>
    );
  }
}


export default class SourceForm extends Component {
  static propTypes = {
    sources: PropTypes.arrayOf(PropTypes.string).isRequired,
    isSidebarOpen: PropTypes.bool.isRequired,
    isTumblrIdeasOpen: PropTypes.bool.isRequired,
    isPastebinIdeasOpen: PropTypes.bool.isRequired,
    isRunning: PropTypes.bool.isRequired,
    shouldPauseForLostFocus: PropTypes.bool.isRequired,
    timingFunction: PropTypes.string.isRequired,
    pastebinId: PropTypes.string,
    includeGifs: PropTypes.bool.isRequired,
    includeStills: PropTypes.bool.isRequired,
    minimumImageWidth: PropTypes.number.isRequired,
    favorites: PropTypes.object.isRequired,
    history: PropTypes.array.isRequired,
  };

  render() {
    const openDirectory = store.updater({
      isTumblrIdeasOpen: !this.props.isTumblrIdeasOpen,
      isHistoryOpen: false,
      isPastebinIdeasOpen: false,
      isRunning: false,
    });
    const openHistory = store.updater({
      isHistoryOpen: !this.props.isHistoryOpen,
      isTumblrIdeasOpen: false,
      isPastebinIdeasOpen: false,
      isRunning: false,
    });
    const openPastebin = store.updater({
      isHistoryOpen: false,
      isTumblrIdeasOpen: false,
      isPastebinIdeasOpen: !this.props.isPastebinIdeasOpen,
      isRunning: false,
    });

    const play = store.updater({
      isRunning: true,
      isTumblrIdeasOpen: false,
      isPastebinIdeasOpen: false,
      isHistoryOpen: false,
    }, (newState) => {
      const ga = window.ga; if (!ga) return;
      newState.sources.forEach((tumblr) => {
          ga('send', {
            hitType: 'event',
            eventCategory: 'Blogs',
            eventAction: 'view',
            eventLabel: tumblr,
          }); 
      });
      ga('send', {
        hitType: 'event',
        eventCategory: 'Playlists',
        eventAction: 'view',
        eventLabel: newState.sources.join(' ')
      });
      if (newState.pastebinId) {
        ga('send', {
          hitType: 'event',
          eventCategory: 'Pastebin',
          eventAction: 'view',
          eventLabel: newState.pastebinId,
        });
      }
    });

    const pause = store.setter('isRunning', false);
    const canPlay = this.props.sources.length > 0;
    const playClassName =
      canPlay ? "gn-playpause gn-play" : "gn-playpause gn-play disabled";
    const toggleFocusBehavior =
      store.setter('shouldPauseForLostFocus',
                   !this.props.shouldPauseForLostFocus);
    const togglePauseBehavior =
      store.setter('shouldShowHelpWhilePaused',
                   !this.props.shouldShowHelpWhilePaused);
    return (
      <div className="gn-form fill-container">
        {this.props.isSidebarOpen && (
          <div className="sidebar-toggle-close hoverable"
               onClick={store.updater({
                isSidebarOpen: false,
                isHistoryOpen: false,
                isTumblrIdeasOpen: false,
                isPastebinIdeasOpen: false,
               })}>
            close
          </div>
        )}

        <div className="gn-form-entry gn-directory-toggle" onClick={openDirectory}>Tumblr ideas</div>
        <div className="gn-form-entry gn-directory-toggle" onClick={openPastebin}>Hastebin Ideas</div>
        <div className="gn-form-entry gn-directory-toggle" onClick={openHistory}>History</div>
        {!this.props.isRunning && (
          <div className={playClassName} onClick={canPlay && play}>▶</div>
        )}
        {this.props.isRunning && (
          <div className="gn-playpause gn-pause" onClick={pause}>❚❚</div>
        )}
        <TimingFunctionPicker timingFunction={this.props.timingFunction} />
        <GifsOrStillsPicker {...this.props} />
        <form className="gn-form-entry">
          <label onClick={toggleFocusBehavior}>
            <input type="checkbox"
                   onChange={() => { }}
                   style={{pointerEvents: 'none'}}
                   checked={this.props.shouldPauseForLostFocus}
                   /> Pause on lost focus
          </label>
        </form>
        <form className="gn-form-entry">
          <label onClick={togglePauseBehavior}>
            <input type="checkbox"
                   onChange={() => { }}
                   style={{pointerEvents: 'none'}}
                   checked={this.props.shouldShowHelpWhilePaused}
                   /> Show help while paused
          </label>
        </form>
        <div className="gn-form-entry gn-form-image-width">
          Min image width: <input type="text"
                                  value={this.props.minimumImageWidth}
                                  onChange={store.numberSetter('minimumImageWidth')} />
        </div>
        <div className="gn-form-entry">
          <input type="text"
                 placeholder="Hastebin ID"
                 value={this.props.pastebinId ? this.props.pastebinId : ""}
                 onChange={store.inputSetter('pastebinId')} />
        </div>
        <div className="gn-sources">
          <SourceTextInput sources={this.props.sources} />
          {this.props.sources.map((name) => {
            return <Source key={name} source={name} isFavorite={this.props.favorites[name]} />;
          })}
        </div>
        {!!this.props.sources.length && <div className="gn-form-entry hoverable"
             style={{textAlign: 'center', fontSize: 12, cursor: 'pointer'}}
             onClick={store.updater({
              isRunning: false,
              sources: [],
             })}>
          clear tumblrs
        </div>}
      </div> 
    );
  }
}
