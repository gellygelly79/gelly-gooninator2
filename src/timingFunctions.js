export default {
  assault: () => 0,
  variableMedium: () => (Math.sin(Date.now() / 1000) + 1) / 2 * 1000 + 200,
  variableFast: () => (Math.sin(Date.now() / 1000) + 1) / 2 * 600,
  variableSlow: () => (Math.sin(Date.now() / 1000) + 1) / 2 * 2000 + 3000,
  variableSlowest: () => (Math.sin(Date.now() / 1000) + 1) / 2 * 3000 + 3500,
  fixed05: () => 500,
  fixed1: () => 1000,
  fixed2: () => 2000,
  cockzoom: () => 5000,
  cockzoomv2: () => 2000,
  cockzoomv3: () => 1000,
};
