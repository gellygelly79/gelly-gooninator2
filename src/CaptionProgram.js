import React, {PropTypes, Component} from "react";
import getRandomListItem from "./getRandomListItem";
const $ = window.$;

var STYLES = {};

var programCounter = 0;
var PROGRAM = [];
var BLINK_DURATION = 200;
var BLINK_DELAY = 80;
var BLINK_GROUP_DELAY = 1200;
var CAPTION_DURATION = 2000;
var CAPTION_DELAY = 1200;
var PHRASES = [];

var splitFirstWord = function(s) {
  var firstSpaceIndex = s.indexOf(" ");
  if (firstSpaceIndex > 0 && firstSpaceIndex < s.length - 1) {
    var first = s.substring(0, firstSpaceIndex);
    var rest = s.substring(firstSpaceIndex + 1);
    return [first, rest];
  } else {
    return [null, null];
  }
}

var getFirstWord = function(s) {
  return splitFirstWord(s)[0];
}

var getRest = function(s) {
  return splitFirstWord(s)[1];
}

var fnIntArg = function(innerFn) {
  return function(el, value) {
    var ms = parseInt(value, 10);
    if (isNaN(ms)) { return null; }
    return innerFn(ms);
  };
}

var COMMANDS = {
  saveStyleRules: function(el, value) {
    var firstWord = getFirstWord(value);
    var style = getRest(value);
    if (!firstWord || !style) { return null; }

    STYLES[firstWord] = STYLES[firstWord] || '';
    STYLES[firstWord] += style;

    return function(f) { f() };
  },

  applySavedStyle: function(el, value) {
    return function(runNextCommand) {
      el.style.cssText = STYLES[value];
      runNextCommand();
    }
  },

  showText: function(el, value) {
    var msString = getFirstWord(value);
    var textString = getRest(value);
    var ms = parseInt(msString, 10);
    if (!textString || isNaN(ms)) { return null; }

    if (textString === '$RANDOM_PHRASE') {
      textString = getRandomListItem(PHRASES);
    }

    return function(runNextCommand) {
      el.style.opacity = 1.0;
      el.innerHTML = textString;
      setTimeout(function() {
        el.style.opacity = 0.0;
        runNextCommand();
      }, ms);
    }
  },

  wait: fnIntArg(function(ms) {
    return function(runNextCommand) { setTimeout(runNextCommand, ms); }
  }),

  setBlinkDuration: fnIntArg(function(ms) {
    return function(runNextCommand) {
      BLINK_DURATION = ms;
      runNextCommand();
    }
  }),

  setBlinkDelay: fnIntArg(function(ms) {
    return function(runNextCommand) {
      BLINK_DELAY = ms;
      runNextCommand();
    }
  }),

  setBlinkGroupDelay: fnIntArg(function(ms) {
    return function(runNextCommand) {
      BLINK_GROUP_DELAY = ms;
      runNextCommand();
    }
  }),

  setCaptionDuration: fnIntArg(function(ms) {
    return function(runNextCommand) {
      CAPTION_DURATION = ms;
      runNextCommand();
    }
  }),

  setCaptionDelay: fnIntArg(function(ms) {
    return function(runNextCommand) {
      CAPTION_DELAY = ms;
      runNextCommand();
    }
  }),

  storePhrase: function(el, value) {
    PHRASES.push(value);
    return function(f) { f() };
  },

  blink: function(el, value) {
    return function(runNextCommand) {
      var fns = [];
      var i = 0;
      el.className = "text-blink";
      value.split('/').forEach(function(word) {
        word = $.trim(word);
        var j = i;
        i += 1;
        fns.push(function() {
          var showText = COMMANDS.showText(el, BLINK_DURATION + ' ' + word);
          var wait = COMMANDS.wait(el, '' + BLINK_DELAY);
          showText(function() { wait(fns[j + 1]); });
        })
      });
      var lastWait = COMMANDS.wait(el, '' + BLINK_GROUP_DELAY);
      fns.push(function() {
        lastWait(runNextCommand);
      });
      fns[0]();
    }
  },

  cap: function(el, value) {
    var showText = COMMANDS.showText(el, CAPTION_DURATION + ' ' + value);
    var wait = COMMANDS.wait(el, '' + CAPTION_DELAY);
    return function(runNextCommand) {
      el.className = "text-caption";
      showText(function() { wait(runNextCommand); });
    }
  },

  bigcap: function(el, value) {
    var showText = COMMANDS.showText(el, CAPTION_DURATION + ' ' + value);
    var wait = COMMANDS.wait(el, '' + CAPTION_DELAY);
    return function(runNextCommand) {
      el.className = "text-caption-big";
      showText(function() { wait(runNextCommand); });
    }
  }
}

var run = function(getHasStopped) {
  if (getHasStopped()) {
    return;
  }
  PROGRAM[programCounter](function() {
    programCounter += 1;
    if (programCounter >= PROGRAM.length) {
      programCounter = 0;
    }
    run(getHasStopped);
  })
}


var startText = function(el, programText) {
  var i = -1;
  var hasError = false;
  programText.split('\n').forEach(function(line) {
    line = $.trim(line);
    i += 1;
    if (line.length < 1) return;

    if (line[0] === '#') {
      return;
    }
    var command = getFirstWord(line);
    if (command) {
      var value = getRest(line);
      if (COMMANDS[command]) {
        var fn = COMMANDS[command](el, value);
        if (fn) {
          PROGRAM.push(fn);
        } else {
          hasError = true;
          console.error("Error on line", i, "- invalid arguments");
        }
      } else {
        hasError = true;
        console.error("Error on line", i, "- unknown command");
      }
    }
  });

  var hasStopped = false;
  var getHasStopped = () => { return hasStopped; };
  if (!hasError) {
    run(getHasStopped);
  }
  return () => { hasStopped = true; };
}


const startShowingText = function(el, pastebinId) {
  if (pastebinId === 'test') {
    var testProgram = `
    setBlinkDuration 300
    setBlinkDelay 100
    setBlinkGroupDelay 1200
    setCaptionDuration 2000
    setCaptionDelay 1200

    bigcap YOU LOVE FLUFFY KITTENS
    blink KITTENS / ARE / YOUR / LIFE
    cap Cuddle all the kittens forever because you love them.
    `
    console.log(testProgram);
    return startText(el, testProgram);
  }

  // var url = `text/${pastebinId}.txt`;
  // var url = 'https://crossorigin.me/http://pastebin.com/raw/' + pastebinId;
  // var url = 'http://cors-proxy.htmldriven.com/?url=http://pastebin.com/raw/' + pastebinId;
  // var url = 'http://cors-proxy.htmldriven.com/?url=https://hastebin.com/raw/' + pastebinId;
  var url = 'https://cors-anywhere.herokuapp.com/https://hastebin.com/raw/' + pastebinId;

  var _hasStoppedEarly = false;
  var _stop = () => {
    _hasStoppedEarly = true;
  };
  var stop = () => { _stop(); };
  $.ajax({
    url: url,
    method: 'get',
    success: function(data) {
      if (_hasStoppedEarly) return;
      if (data.body) {
        data = data.body;
      }
      console.log(data);
      _stop = startText(el, data);
    },
    fail: function(err) {
      console.error("Could not load hastebin ID", pastebinId)
      alert("Could not load your hastebin. This may or may not be temporary.");
    },
  });
  return stop;
};


export default class CaptionProgram extends Component {
    static propTypes = {
        pastebinId: PropTypes.string.isRequired,
    }

    constructor() {
        super();
        this.stop = null;
        this.lastPastebinId = null;
    }

    shouldComponentUpdate(nextProps, nextState) {
        return nextProps.pastebinId !== this.props.pastebinId;
    }

    componentDidMount() {
        this._update(this.props);
    }

    componentWillReceiveProps(nextProps) {
        this._update(nextProps);
    }

    componentWillUnmount() {
        this.stop();
    }

    _update(props) {
        if (!this.el) return;
        if (props.pastebinId === this.lastPastebinId) return;
        this.lastPastebinId = props.pastebinId;
        this._stop();

        this.stop = startShowingText(this.el, props.pastebinId);
    }

    _stop() {
        if (this.stop) {
            this.stop()
            this.stop = null;
        }
    }

    render() {
      return (
        <div style={{pointerEvents: 'none', display: 'table', width: '100%', height: '100%'}}
             className="fill-container text-display">
          <div style={{display: 'table-cell'}} ref={(el) => this.el = el} />
        </div>
      );
    }
}