const $ = window.$;
import API_KEY from "./apiKey";

export default class Fetcher {
  constructor(source) {
    this.source = source;
    this.allURLs = [];
    this.gifURLs = [];
    this.stillURLs = [];
    this.sizesByURL = {};
    this.start();
  }

  start() {
    this._fetch(0);
  }

  _fetch(offset) {
    let isFaves = false;
    let url = (
      `https://api.tumblr.com/v2/blog/${this.source}.tumblr.com/posts?` +
      `offset=${offset}&limit=20&` +
      `api_key=${API_KEY}`
    );
    if (this.source.indexOf('/') !== -1) {
      const [realSource, tag] = this.source.split('/');
      if (tag === "faves") {
        isFaves = true;
        url = (
          `http://api.tumblr.com/v2/blog/${realSource}.tumblr.com/likes?` +
          `offset=${offset}&limit=20&` +
          `api_key=${API_KEY}`
        );
      } else {
        url = (
          `http://api.tumblr.com/v2/blog/${realSource}.tumblr.com/posts?` +
          `offset=${offset}&limit=20&tag=${tag}&` +
          `api_key=${API_KEY}`
        );
      }
    }

    $.ajax({
      method: 'GET',
      cache: false,
      dataType: 'jsonp',
      url: url,
      error: (err) => {
        console.error(err);
      },
      success: (data) => {
        if (data.meta.status !== 200 || offset >= data.response.total_posts || offset > 500) { return; }

        const posts = isFaves ? data.response.liked_posts : data.response.posts;
        posts.forEach((post, i) => {
          if (!post.photos) return;
          post.photos.forEach((photo) => {
            const url = photo.original_size.url;
            this.sizesByURL[url] = photo.original_size;

            this.allURLs.push(url);
            if (url.endsWith('.gif')) {
              this.gifURLs.push(url);
            } else {
              this.stillURLs.push(url);
            }
          });
        });
        this._fetch(offset + 20);
      }
    });
  }
}