import React, { Component, PropTypes } from 'react';
import store from "./Store";


export default class HistoryDirectory extends Component {
  static propTypes = {
    history: PropTypes.array.isRequired,
  };

  render() {
    const seen = {};
    return (
      <div className="fill-container gn-ui-container">
        {this.props.history.filter((e) => e.sources).reverse().map((entry, i) => {
          const json = JSON.stringify(entry);
          if (seen[json]) return null;
          seen[json] = true;
          const differingKeys = store.urlKeys()
            .filter((k) => {
              if (k === 'sources' || k === 'isRunning') return false;
              return store.isValueDifferentFromDefault(k, entry[k]);
            });
          return (
            <div onClick={store.updater({...entry, isRunning: true, isHistoryOpen: false})}
                 key={i}
                 className="gn-directory-entry gn-history-entry">
                <div>{entry.sources.join(' ')}</div>
                <div style={{fontSize: 12}}>
                  {differingKeys.map((k) => {
                    return <span key={k}>{k}: {entry[k]}</span>
                  })}
                </div>
            </div>
          );
        })}
      </div>
    );
  }
}
